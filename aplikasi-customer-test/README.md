## Menjalankan Selenium Grid sebagai Hub ##

Selenium Server bisa diunduh [di sini](https://www.seleniumhq.org/download/)

```
java -jar selenium-server-standalone-2.44.0.jar -role hub
```

## Menjalankan Selenium Grid sebagai Node ##

```
java -jar selenium-server-standalone-2.44.0.jar -role node -host <alamat-ip-node> -hub http://<ip-hub>:<port-hub>/grid/register/
```


## Soal Post Test ##

[https://goo.gl/forms/u6PSyCddLgH5sFQ53](https://goo.gl/forms/u6PSyCddLgH5sFQ53)