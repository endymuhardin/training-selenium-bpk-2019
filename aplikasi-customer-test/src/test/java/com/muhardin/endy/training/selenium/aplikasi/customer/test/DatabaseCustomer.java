package com.muhardin.endy.training.selenium.aplikasi.customer.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseCustomer {
    private static final String SQL_HAPUS_DATA = "delete from customer";
    private static final String SQL_INSERT_DATA = "insert into customer "
            + "(id, fullname, email, mobile_phone, birthdate, gender, education) "
            + "values (?,?,?,?,?,?,?)";
    
    private String databaseDriver;
    private String databaseUrl;
    private String databaseUsername;
    private String databasePassword;
    
    private Connection koneksi;

    public DatabaseCustomer(String databaseDriver, String databaseUrl, String databaseUsername, String databasePassword) {
        this.databaseDriver = databaseDriver;
        this.databaseUrl = databaseUrl;
        this.databaseUsername = databaseUsername;
        this.databasePassword = databasePassword;
    }
    
    public void connect(){
        try {
            koneksi = DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void disconnect() {
        try {
            koneksi.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void hapusData(){
        try {
            koneksi.prepareStatement(SQL_HAPUS_DATA).executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void isiData(String filename){
        try {
            PreparedStatement psInsert = koneksi.prepareStatement(SQL_INSERT_DATA);
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(filename)));
            String data = reader.readLine(); // skip header
            while((data = reader.readLine()) != null){
                String[] customerData = data.split(",");
                psInsert.setString(1, UUID.randomUUID().toString()); //id
                psInsert.setString(2, customerData[0]); //fullname
                psInsert.setString(3, customerData[1]); //email
                psInsert.setString(4, customerData[2]); //hp
                psInsert.setString(5, customerData[3]); //birthdate
                psInsert.setString(6, customerData[4]); //gender
                psInsert.setString(7, customerData[5]); //education
                psInsert.executeUpdate();
            }
        } catch (IOException | SQLException ex) {
            Logger.getLogger(DatabaseCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
