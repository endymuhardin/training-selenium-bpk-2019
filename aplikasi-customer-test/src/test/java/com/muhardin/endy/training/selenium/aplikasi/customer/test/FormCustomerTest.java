package com.muhardin.endy.training.selenium.aplikasi.customer.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.muhardin.endy.training.selenium.aplikasi.customer.test.pageobject.FormCustomer;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@RunWith(JUnitParamsRunner.class)
public class FormCustomerTest {
    private static WebDriver webDriver;
    private static DatabaseCustomer databaseCustomer;
    
    private static String dbUsername = "bece3af9438e47";
    private static String dbPassword = "53a4168e";
    private static String dbUrl = "jdbc:mysql://us-cdbr-iron-east-03.cleardb.net/heroku_b5558f115cf4f67";
    private static String urlResetDatabase = "http://training-selenium-bpk.herokuapp.com/devtools/db/customer";
    
    private static String urlSeleniumGridHub = "http://192.168.8.101:4444/wd/hub";
    
    private Path lokasiFolderScreenshot = 
            Paths.get(System.getProperty("user.home"),"selenium-screenshot") ;
    
    private String urlAplikasi = "http://training-selenium-bpk.herokuapp.com/customer/form";
    private String[] pilihanJenisKelamin = {"Pria", "Wanita"};
    private String[] pilihanPendidikan = {"SD", "SMP", "SMUSMK", "Sarjana"};
    
    @BeforeClass
    public static void jalankanBrowser() throws MalformedURLException{
        databaseCustomer = new DatabaseCustomer("com.mysql.jdbc.Driver", dbUrl, dbUsername, dbPassword);
        //databaseCustomer.connect();
        
        // menjalankan webdriver di local
        webDriver = new ChromeDriver();
        //webDriver = new FirefoxDriver();
        // webDriver = new SafariDriver();
        
        // menjalankan webdriver dengan selenium grid
        //webDriver = new RemoteWebDriver(new URL(urlSeleniumGridHub), DesiredCapabilities.firefox());
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
        //databaseCustomer.disconnect();
    }
    
    //@Before
    public void resetDatabase(){
        System.out.println("Reset database customer");
        databaseCustomer.hapusData();
        databaseCustomer.isiData("/sample-customer.csv");
    }
    
    @Before
    public void resetDatabaseViaHttp(){
        
        try {
            Map<String,String>[] sampleDataCustomer = new ObjectMapper()
                    .readValue(
                            FormCustomerTest.class
                                    .getResourceAsStream("/sample-customer.json"),
                            Map[].class);
            
            WebClient.create().post()
                    .uri(urlResetDatabase)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .body(Mono.just(sampleDataCustomer), Object.class)
                    .retrieve().bodyToMono(Void.class).block();
        } catch (IOException ex) {
            Logger.getLogger(FormCustomerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Test
    public void testInputSukses() throws Exception {
        Random rand = new Random();
        // data yang mau diinput
        Faker faker = new Faker(new Locale("in", "id"));
        String namaDepan = faker.name().firstName();
        String namaBelakang = faker.name().lastName();
        String email = namaDepan + "." + namaBelakang + "@" + faker.internet().domainName();
        String hp = faker.phoneNumber().cellPhone();
        String jenisKelamin = pilihanJenisKelamin[rand.nextInt(pilihanJenisKelamin.length)];
        String pendidikan = pilihanPendidikan[rand.nextInt(pilihanPendidikan.length)];
        
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String ultah = formatter.format(faker.date().birthday());
        
        webDriver.get(urlAplikasi);
        FormCustomer fc = new FormCustomer(webDriver);
        
        fc.isiNama(namaDepan + " " + namaBelakang);
        fc.isiEmail(email);
        fc.isiNoHp(hp);
        fc.isiTanggalLahir(ultah);
        fc.isiJenisKelamin(jenisKelamin);
        fc.pilihPendidikan(pendidikan);
        
        //Thread.sleep(10 * 1000);  // tunggu 10 detik
        fc.simpan();
        
        ExpectedCondition<Boolean> pageLoadCondition = 
                (WebDriver driver) -> driver.getTitle().equals("Data Customer");
        
        // tunggu 30 detik, cek apakah berada di halaman Data Customer
        WebDriverWait wait = new WebDriverWait(webDriver, 30); 
        wait.until(pageLoadCondition);
        
    }
    
    @Test
    @FileParameters("classpath:test-customer.csv")
    public void testInvalidInput(String nama, String email, 
            String hp, String tgl, String gender, 
            String pendidikan, String idElementError, String pesanError) throws IOException{
        
        webDriver.get(urlAplikasi);
        FormCustomer fc = new FormCustomer(webDriver);
        
        fc.isiNama(nama);
        fc.isiEmail(email);
        fc.isiNoHp(hp);
        fc.isiTanggalLahir(tgl);
        fc.isiJenisKelamin(gender);
        fc.pilihPendidikan(pendidikan);
        
        //Thread.sleep(10 * 1000);  // tunggu 10 detik
        fc.simpan();
        
        ExpectedCondition<Boolean> pageLoadCondition = 
                (WebDriver driver) -> driver.getTitle().equals("Edit Customer");
        
        // tunggu 30 detik, cek apakah berada di halaman Data Customer
        WebDriverWait wait = new WebDriverWait(webDriver, 30); 
        wait.until(pageLoadCondition);
        
        screenshot(webDriver);
        
        Assert.assertNotNull("Pesan error ditampilkan",
                webDriver.findElement(By.id(idElementError)));
        
        Assert.assertTrue("Harusnya pesan error "+pesanError+" ada dalam page", 
                webDriver.getPageSource().contains(pesanError));
    }
    
    public void screenshot(WebDriver webDriver) throws IOException {
        TakesScreenshot ts = (TakesScreenshot) webDriver;
        byte[] scr = ts.getScreenshotAs(OutputType.BYTES);
        
        Files.createDirectories(lokasiFolderScreenshot);
        Files.write(Paths.get(lokasiFolderScreenshot.toString(), 
                UUID.randomUUID().toString()+".png"),scr);
    }
}
