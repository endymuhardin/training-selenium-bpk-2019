package com.muhardin.endy.training.selenium.aplikasi.customer.test;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InputCustomerTests {
    
    private static WebDriver webDriver;
    
    private String urlAplikasi = "http://training-selenium-bpk.herokuapp.com/customer/form";
    
    @BeforeClass
    public static void jalankanBrowser(){
        webDriver = new ChromeDriver();
        // webDriver = new FirefoxDriver();
        // webDriver = new SafariDriver();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    @Test
    public void testInputSukses() throws Exception {
        System.out.println("Test input customer sukses");
        
        webDriver.get(urlAplikasi);
        
        webDriver.findElement(By.name("fullname")).sendKeys("Test User 001");
        webDriver.findElement(By.name("email")).sendKeys("user001@contoh.com");
        webDriver.findElement(By.name("mobilePhone")).sendKeys("081298000468");
        webDriver.findElement(By.id("gender2")).click();
        
        // combo box / dropdown list
        Select comboPendidikan = new Select(webDriver.findElement(By.name("education")));
        comboPendidikan.selectByValue("Magister");
        
        webDriver.findElement(By.name("birthdate")).sendKeys("2011-11-30");
        webDriver.findElement(By.id("simpanCustomer")).click();
        
        ExpectedCondition<Boolean> pageLoadCondition = 
                (WebDriver driver) -> driver.getTitle().equals("Data Customer");
        
        // tunggu 30 detik, cek apakah berada di halaman Data Customer
        WebDriverWait wait = new WebDriverWait(webDriver, 30); 
        wait.until(pageLoadCondition);
    }
    
    @Test
    public void testInputEmailKosong(){
        System.out.println("Test input email kosong");
        
        webDriver.get(urlAplikasi);
    }
    
    @Test
    public void testInputEmailSalahFormat(){
        System.out.println("Test input email salah format");
        
        webDriver.get(urlAplikasi);
    }
}
