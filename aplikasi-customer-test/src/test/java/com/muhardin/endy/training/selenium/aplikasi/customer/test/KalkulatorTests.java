package com.muhardin.endy.training.selenium.aplikasi.customer.test;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class KalkulatorTests {
    
    @Test
    public void testTambah(){
        // Jalankan aplikasinya
        Kalkulator k = new Kalkulator();
        
        // ekspektasi 4 + 3 = 7
        int hasil = k.tambah(4, 3);
        
        System.out.println("4 + 3 = "+hasil);
        
        // bandingkan antara expectation vs actual
        Assert.assertEquals(7, hasil);
        Assert.assertEquals(9, k.tambah(4,5));
    }
    
    @Test
    public void testKurang(){
        // Jalankan aplikasinya
        Kalkulator k = new Kalkulator();
        
        System.out.println("Menjalankan method testKurang");
        
        // bandingkan antara expectation vs actual
        Assert.assertEquals(7, k.kurang(14,7));
        Assert.assertEquals(9, k.kurang(14,5));
    }
    
    @BeforeClass
    public static void sekaliSajaSebelumSemuaTest(){
        System.out.println("Menjalankan method before class");
    }
    
    @Before
    public void sebelumMasingMasingTest(){
        System.out.println("Menjalankan method before");
    }
    
    @After
    public void setelahMasingMasingTest(){
        System.out.println("Menjalankan method after");
    }
    
    @AfterClass
    public static void sekaliSajaSetelahSemuaTest(){
        System.out.println("Menjalankan method after class");
    }
}
