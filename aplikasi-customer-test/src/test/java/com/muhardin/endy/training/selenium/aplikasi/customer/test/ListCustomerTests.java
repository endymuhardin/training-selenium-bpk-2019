package com.muhardin.endy.training.selenium.aplikasi.customer.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.selenium.aplikasi.customer.test.pageobject.ListCustomer;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

public class ListCustomerTests {
    private static WebDriver webDriver;
    private static String urlResetDatabase = "http://training-selenium-bpk.herokuapp.com/devtools/db/customer";
    
    private String urlAplikasi = "http://training-selenium-bpk.herokuapp.com/customer/list";
    
    @BeforeClass
    public static void jalankanBrowser(){
        //webDriver = new ChromeDriver();
        webDriver = new FirefoxDriver();
        // webDriver = new SafariDriver();
    }
    
    @AfterClass
    public static void tutupBrowser(){
        webDriver.quit();
    }
    
    //@Before
    public void resetDatabaseViaHttp(){
        
        try {
            Map<String,String>[] sampleDataCustomer = new ObjectMapper()
                    .readValue(
                            FormCustomerTest.class
                                    .getResourceAsStream("/sample-customer.json"),
                            Map[].class);
            
            WebClient.create().post()
                    .uri(urlResetDatabase)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .accept(MediaType.APPLICATION_JSON_UTF8)
                    .body(Mono.just(sampleDataCustomer), Object.class)
                    .retrieve().bodyToMono(Void.class).block();
        } catch (IOException ex) {
            Logger.getLogger(FormCustomerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    @Test
    public void testHapusCustomerPalingAtas(){
        webDriver.get(urlAplikasi);
        ListCustomer listCustomerScreen = new ListCustomer(webDriver);
        
        // ambil dulu, buat diperiksa setelah dihapus
        String namaCustomer = listCustomerScreen.ambilNamaCustomerPalingAtas();
        System.out.println("Nama Customer yang mau dihapus : "+namaCustomer);
        
        // hapus record
        listCustomerScreen.hapusCustomerPalingAtas();
        
        // tunggu 30 detik, cek apakah berada di halaman Data Customer
        ExpectedCondition<Boolean> pageLoadCondition = 
                (WebDriver driver) -> driver.getTitle().equals("Data Customer");
        
        WebDriverWait wait = new WebDriverWait(webDriver, 30); 
        wait.until(pageLoadCondition);
        
        // cek harusnya nama customer yang dihapus sudah tidak ada
        Assert.assertFalse("Customer "+namaCustomer+" harusnya sudah tidak ada", 
                webDriver.getPageSource().contains(namaCustomer));
    }
    
    @Test
    public void testHapusSemua(){
        webDriver.get(urlAplikasi);
        ListCustomer listCustomerScreen = new ListCustomer(webDriver);
        String jumlah = listCustomerScreen.hitungJumlahCustomer();
        for(int i=0; i<20; i++) {
            listCustomerScreen.hapusCustomerPalingAtas();
        }
    }
}
