package com.muhardin.endy.training.selenium.aplikasi.customer.test.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FormCustomer {
    
    @FindBy(name = "fullname")
    WebElement namaLengkap;
    
    @FindBy(name = "email")
    WebElement email;
    
    @FindBy(name = "mobilePhone")
    WebElement noHp;
    
    @FindBy(id = "gender1")
    WebElement jenisKelaminPria;
    
    @FindBy(id = "gender2")
    WebElement jenisKelaminWanita;
    
    @FindBy(name = "education")
    WebElement tingkatPendidikan;
    
    @FindBy(name = "birthdate")
    WebElement tanggalLahir;
    
    @FindBy(id = "simpanCustomer")
    WebElement tombolSimpan;
    
    public FormCustomer(WebDriver wd) {
        PageFactory.initElements(wd, this);
    }
    
    public void isiNama(String nama){
        namaLengkap.sendKeys(nama);
    }
    
    public void isiEmail(String x){
        email.sendKeys(x);
    }
    
    public void isiNoHp(String hp){
        noHp.sendKeys(hp);
    }
    
    public void isiJenisKelamin(String jk){
        if("Pria".equalsIgnoreCase(jk) || "Male".equalsIgnoreCase(jk)) {
            jenisKelaminPria.click();
        }
        
        if("Wanita".equalsIgnoreCase(jk) || "Female".equalsIgnoreCase(jk)) {
            jenisKelaminWanita.click();
        }
    }
    
    public void pilihPendidikan(String pd){
        new Select(tingkatPendidikan).selectByValue(pd);
    }
    
    public void isiTanggalLahir(String tgl){
        tanggalLahir.sendKeys(tgl);
    }
    
    public void simpan(){
        tombolSimpan.click();
    }
}
