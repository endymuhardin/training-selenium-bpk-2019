package com.muhardin.endy.training.selenium.aplikasi.customer.test.pageobject;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ListCustomer {
    
    @FindBy(xpath = "/html/body/div/div/main/div/section/div[2]/table/tbody/tr[1]/td[1]")
    WebElement namaCustomerPalingAtas;
    
    @FindBy(xpath = "/html/body/div/div/main/div/section/div[2]/table/tbody/tr[1]/td[7]/a[2]")
    WebElement tombolDeletePalingAtas;
    
    @FindBy(xpath = "/html/body/div/div/main/div/section/div[2]/div/div[1]/div")
    WebElement jumlahCustomer;
    
    public ListCustomer(WebDriver webDriver){
        PageFactory.initElements(webDriver, this);
    }
    
    public String ambilNamaCustomerPalingAtas(){
        return namaCustomerPalingAtas.getText();
    }
    
    public void hapusCustomerPalingAtas(){
        tombolDeletePalingAtas.click();
    }
    
    public String hitungJumlahCustomer(){
        return jumlahCustomer.getText();
    }
}
